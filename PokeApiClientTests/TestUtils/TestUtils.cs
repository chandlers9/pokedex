using System.IO;

namespace PokeApiClientTests.TestUtils
{
    internal static class TestUtils
    {
        internal static string GetSampleJson()
        {
            var path = Directory.GetCurrentDirectory().Replace("/bin/Debug/netcoreapp3.1", null) +
                       "/TestUtils/PokemonSpeciesExampleResponse.json";
            var originalJson = LoadJson(path);
            return originalJson;
        }
        
        private static string LoadJson(
            string path)
        {
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();

                return json;
            }
        }
    }
}