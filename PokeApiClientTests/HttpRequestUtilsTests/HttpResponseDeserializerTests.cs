using System.Linq;
using Common.Failures;
using Common.Utils;
using Newtonsoft.Json;
using NUnit.Framework;
using PokeApiClient.HttpRequestUtils;

namespace PokeApiClientTests.HttpRequestUtilsTests
{
    public class HttpResponseDeserializerTests
    {
        // This is slightly different to the RoundTripSerialisationTest in that it is testing the DeserializeResponse function
        [Test]
        public void DeserializeResponse_RoundTripTest()
        {
            var originalJson = TestUtils.TestUtils.GetSampleJson();
            var deserializedObject = HttpResponseDeserializer
                .DeserializePokemonSpeciesResponse(originalJson)
                .ThrowIfFailed();
            var serializedObject = JsonConvert.SerializeObject(deserializedObject);
            
            Assert.That(originalJson, Is.EqualTo(serializedObject));
        }

        [TestCase("justSomeRandomString")]
        public void DeserializeResponse_WhenNonJsonStringUsed_ReturnsNonJsonStringProvidedFailure(
            string nonJsonResponse)
        {
            var response = HttpResponseDeserializer.DeserializePokemonSpeciesResponse(nonJsonResponse);

            response.Match(
                Right: r => Assert.Fail("This should not have succeed"),
                Left: l => Assert.That(response.Case.GetType() == typeof(NonJsonStringProvidedFailure)));
        }
    }
}