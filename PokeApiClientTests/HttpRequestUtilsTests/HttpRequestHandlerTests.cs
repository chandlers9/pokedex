using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Common.Failures;
using Common.Utils;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using PokeApiClient;
using PokeApiClient.HttpRequestUtils;
using static PokeApiClientTests.TestUtils.TestUtils;

namespace PokeApiServiceTests.HttpRequestUtilsTests
{
    public class HttpRequestHandlerTests
    {
        [TestCase(1)]
        [TestCase(100)]
        [TestCase(199)]
        [TestCase(201)]
        [TestCase(300)]
        [TestCase(400)]
        [TestCase(499)]
        public void Get_WhenStatusCodeIsLowerThan500_AndUnexpected_ThenFailureReturned(
            int statusCode)
        {
            // GIVEN the response code is in the 400s
            var response = new HttpResponseMessage
            {
                StatusCode = (HttpStatusCode) statusCode,
                Content = new StringContent(@""),
            };
            var httpRequestHandler = GetHttpRequestHandlerWithMockedResponse(response);
            
            // WHEN call Get
            var task = httpRequestHandler.Get(ApiRoutes.GET_POKEMON_SPECIES_BASE_URL);
            task.Wait();
            var result = task.Result;

            // THEN UnsuccessfulResponseFailure is returned
            Assert.That(result.Case, Is.TypeOf<UnsuccessfulResponseFailure>());
        }

        [TestCase(500)]
        [TestCase(511)]
        public void Get_WhenUnsuccessfulStatusCodeAbove499Returned_ThenThrowsException(
            int statusCode)
        {
            // GIVEN the response code is in the s
            var response = new HttpResponseMessage
            {
                StatusCode = (HttpStatusCode) statusCode,
                Content = new StringContent(@""),
            };
            var httpRequestHandler = GetHttpRequestHandlerWithMockedResponse(response);

            // WHEN call Get
            var task = httpRequestHandler.Get(ApiRoutes.GET_POKEMON_SPECIES_BASE_URL);
            
            // Task will throw an aggregate exception
            Assert.Throws<AggregateException>(task.Wait);
        }

        [Test]
        public void Get_WhenSuccessfulResponseCodeReturned_ThenReturnsResponseBody()
        {
            // GIVEN the response code is in the 
            var response = new HttpResponseMessage
            {
                StatusCode = (HttpStatusCode) 200,
                Content = new StringContent(GetSampleJson()),
            };
            var httpRequestHandler = GetHttpRequestHandlerWithMockedResponse(response);
            
            // WHEN call Get
            var task = httpRequestHandler.Get(ApiRoutes.GET_POKEMON_SPECIES_BASE_URL);
            task.Wait();
            var result = task
                .Result
                .ThrowIfFailed();

            // THEN response  is returned
            Assert.That(result, Is.TypeOf<string>());
        }

        // Mocking the HttpClient is very hard, on its own.
        // This captures any responses from e.g. _client.GetAsync and mocks them
        private static HttpRequestHandler GetHttpRequestHandlerWithMockedResponse(
            HttpResponseMessage mockedResponse)
        {
            var handlerMock = new Mock<HttpMessageHandler>();
          
            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(mockedResponse);
            var httpClient = new HttpClient(handlerMock.Object);
            return new HttpRequestHandler(httpClient);
        }
    }
}