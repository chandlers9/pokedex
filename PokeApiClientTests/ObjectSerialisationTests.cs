using Newtonsoft.Json;
using NUnit.Framework;
using PokeApiClient;

namespace PokeApiClientTests
{
    public class ObjectSerialisationTests
    {
        // Test that serialising and deserializing the object returns the same value
        [Test]
        public void RoundTripSerialisationTest()
        {
            var originalJson = TestUtils.TestUtils.GetSampleJson();
            PokemonSpeciesResponse deserializedObject = JsonConvert.DeserializeObject<PokemonSpeciesResponse>(originalJson);
            var serializeObject = JsonConvert.SerializeObject(deserializedObject);
            
            Assert.That(originalJson, Is.EqualTo(serializeObject));
        }
    }
}