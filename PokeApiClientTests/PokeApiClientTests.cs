using System.Net;
using Common.Failures;
using LanguageExt;
using Moq;
using NUnit.Framework;
using PokeApiClient.HttpRequestUtils;

namespace PokeApiClientTests
{
    public class PokeApiClientTests
    {
        [Test, Ignore("Mocks require more setup")]
        public void GetPokemonSpecies_ApiReturnsCorrectJson_ThenReturnsInitializedResponseObject()
        {
            // GIVEN api call returns valid json response
            var httpMockHandler = new Mock<HttpRequestHandler>(MockBehavior.Strict);
            httpMockHandler
                .Setup(handler => handler.Get(
                    It.IsAny<string>()))
                .Returns<string>(x => Prelude
                    .Right<IFailure, string>(TestUtils.TestUtils.GetSampleJson())
                    .AsTask());
            var pokeApiClient = new PokeApiClient.PokeApiClient(httpMockHandler.Object);
            
            // WHEN GetPokemonSpecies
            var response = pokeApiClient.GetPokemonSpecies("somePokemonName");
            
            // THEN success
            Assert.That(response.IsRight);
        }
        
        [TestCase("justSomeRandomString"), Ignore("Mocks require more setup")]
        public void GetPokemonSpecies_ApiReturnsInvalidStringInRightState_ThenNonJsonStringProvidedFailure(
            string invalidJsonResponse)
        {
            // GIVEN the request returns a successful either with bad data
            var httpMockHandler = new Mock<HttpRequestHandler>(MockBehavior.Strict);
            httpMockHandler
                .Setup(handler => handler.Get(
                    It.IsAny<string>()))
                .Returns<string>(json => Prelude
                    .Right<IFailure, string>(invalidJsonResponse)
                    .AsTask());
            
            // WHEN GetPokemonSpecies
            var pokeApiClient = new PokeApiClient.PokeApiClient(httpMockHandler.Object);
            
            // WHEN GetPokemonSpecies
            var response = pokeApiClient.GetPokemonSpecies("somePokemonName");
            
            // THEN NonJsonStringProvidedFailure is returned
            Assert.That(response, Is.TypeOf<NonJsonStringProvidedFailure>());
        }
        
        [TestCase("{\"invalid\":\"string\",\"in\":\"2012-10-21T00:00:00+05:30\",\"json\":0}"), Ignore("This case is not handled yet - Mocks require more setup")] 
        public void GetPokemonSpecies_ApiReturnsInvalidStringInJsonFormat_ThenSomeFailureOccurs(
            string invalidJsonResponse)
        {
            // GIVEN the request returns a successful either with bad data
            var httpMockHandler = new Mock<HttpRequestHandler>(MockBehavior.Strict);
            httpMockHandler
                .Setup(handler => handler.Get(
                    It.IsAny<string>()))
                .Returns<string>(json => Prelude
                    .Right<IFailure, string>(invalidJsonResponse)
                    .AsTask());
            
            // WHEN GetPokemonSpecies
            var pokeApiClient = new PokeApiClient.PokeApiClient(httpMockHandler.Object);
            
            // WHEN GetPokemonSpecies
            var response = pokeApiClient.GetPokemonSpecies("somePokemonName");

            // THEN.....
        }

        [Test, Ignore("Mocks require more setup")]
        public void GetPokemonSpecies_ApiReturnsFailure_ThenPropagatesFailure()
        {
            // GIVEN the request returns a successful either with bad data
            var httpMockHandler = new Mock<HttpRequestHandler>(MockBehavior.Strict);
            httpMockHandler
                .Setup(handler => handler.Get(
                    It.IsAny<string>()))
                .Returns<string>(json => Prelude
                    .Left<IFailure, string>(new UnsuccessfulResponseFailure(
                        "msg",
                        HttpStatusCode.BadRequest))
                    .AsTask());
            
            // WHEN GetPokemonSpecies
            var pokeApiClient = new PokeApiClient.PokeApiClient(httpMockHandler.Object);
            
            // WHEN GetPokemonSpecies
            var response = pokeApiClient.GetPokemonSpecies("somePokemonName");

            // THEN failure is propagated
            Assert.That(response.Case, Is.TypeOf<UnsuccessfulResponseFailure>());
        }
    }
}