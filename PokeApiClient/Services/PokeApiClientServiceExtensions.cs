using Microsoft.Extensions.DependencyInjection;
using PokeApiClient.HttpRequestUtils;

namespace PokeApiClient.Services
{
    /// <summary>
    /// Can be used as an easy way to register the required services for this class library in net core apps. 
    /// </summary>
    public static class PokeApiClientServiceExtensions
    {
        public static IServiceCollection AddPokeApiClientServices(
            this IServiceCollection services)
        {
            services.AddHttpClient<IHttpRequestHandler, HttpRequestHandler>();
            services.AddScoped<IPokeApiClient, PokeApiClient>();
            
            return services;
        }
        
    }
}