using System;
using Common.Failures;
using LanguageExt;
using PokeApiClient.HttpRequestUtils;

namespace PokeApiClient
{
    public class PokeApiClient : IPokeApiClient
    {
        private readonly IHttpRequestHandler _httpRequestHandler;
        public PokeApiClient(
            IHttpRequestHandler httpRequestHandler)
        {
            _httpRequestHandler = httpRequestHandler ?? throw new ArgumentNullException(nameof(httpRequestHandler));
        }
        
        public Either<IFailure, PokemonSpeciesResponse> GetPokemonSpecies(
            string pokemonName)
        {
            // Make HTTP request
            var task = _httpRequestHandler.Get($"{ApiRoutes.GET_POKEMON_SPECIES_BASE_URL}{pokemonName}");
            task.Wait();

            return
                from jsonResponse in task.Result
                from pokemonSpeciesResponse in HttpResponseDeserializer.DeserializePokemonSpeciesResponse(jsonResponse)
                select pokemonSpeciesResponse;
        }
    }
}