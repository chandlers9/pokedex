using Common.Failures;
using LanguageExt;

namespace PokeApiClient
{
    public interface IPokeApiClient
    {
        public Either<IFailure, PokemonSpeciesResponse> GetPokemonSpecies(
            string pokemonName);
    }
}