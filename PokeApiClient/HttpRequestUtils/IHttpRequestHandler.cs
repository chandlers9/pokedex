using System.Threading.Tasks;
using Common.Failures;
using LanguageExt;

namespace PokeApiClient.HttpRequestUtils
{
    public interface IHttpRequestHandler
    {
        public Task<Either<IFailure, string>> Get(string uri);
    }
}