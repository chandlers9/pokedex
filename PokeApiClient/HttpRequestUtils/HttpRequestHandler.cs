using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Common.Failures;
using LanguageExt;

namespace PokeApiClient.HttpRequestUtils
{
    public class HttpRequestHandler : IHttpRequestHandler
    {
        private readonly HttpClient _client;

        public HttpRequestHandler(
            HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<Either<IFailure, string>> Get(
            string uri)
        {
            HttpResponseMessage response = await _client.GetAsync(uri);
            string responseBody = await response.Content.ReadAsStringAsync();

            return
                from _ in HandleResponseCodes(response)
                select responseBody;
            
            // Local functions
            static Either<IFailure, Unit> HandleResponseCodes(
                HttpResponseMessage response)
            {
                // Expected 200 OK
                if (response.StatusCode == (HttpStatusCode) 200)
                {
                    return Unit.Default;
                }
                
                // Unexpected status codes (anything below 500 which isn't 200)
                if (response.StatusCode < (HttpStatusCode) 500)
                {
                    return new UnsuccessfulResponseFailure(
                        response.ReasonPhrase,
                        response.StatusCode);
                }
            
                // If any other response code, throw exception (cause internal server error)
                // Would be able to pick this up in logs when deployed
                throw new HttpRequestException($"GetPokemon Api Call. " +
                                               $"Status code: {response.StatusCode}");
            }
        }
    }
}