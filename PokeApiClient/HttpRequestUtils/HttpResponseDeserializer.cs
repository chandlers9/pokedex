using Common.Failures;
using LanguageExt;
using Newtonsoft.Json;

namespace PokeApiClient.HttpRequestUtils
{
    public static class HttpResponseDeserializer
    {
        public static Either<IFailure, PokemonSpeciesResponse> DeserializePokemonSpeciesResponse(
            string json)
        {
            try
            {
                var deserializedResponse =
                    JsonConvert.DeserializeObject<PokemonSpeciesResponse>(json);
                
                return deserializedResponse;
            }
            catch (JsonReaderException e) 
            {
                return new NonJsonStringProvidedFailure(
                    functionName: $"{nameof(DeserializePokemonSpeciesResponse)}",
                    providedValue: json
                );
            }
        }
    }
}