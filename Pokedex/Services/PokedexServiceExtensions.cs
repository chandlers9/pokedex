using Microsoft.Extensions.DependencyInjection;

namespace Pokedex.Services
{
    public static class PokemonServiceExtensions
    {
        /// <summary>
        /// Register Pokedex related services
        /// </summary>
        public static IServiceCollection AddPokedexServices(
            this IServiceCollection services)
        {
            services.AddScoped<IPokedexApi, PokedexApi>();
            services.AddScoped<IPokedexCatalogue, PokedexCatalogue>();
            services.AddScoped<IPokedexView, PokedexView>();
            
            return services;
        }
    }
}