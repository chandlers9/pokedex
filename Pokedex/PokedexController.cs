using Microsoft.AspNetCore.Mvc;

namespace Pokedex
{
    [Controller]
    public class PokedexController : Controller
    {
        private IPokedexApi PokedexApi;

        public PokedexController(
            IPokedexApi pokedexApi)
        {
            PokedexApi = pokedexApi;
        }

        [HttpGet]
        [Route("pokemon/{pokemonName}")]
        public ActionResult<string> GetPokemon(
            string pokemonName)
        {
            return $"{nameof(GetPokemon)} => {pokemonName}";
        }
    }
}


   