namespace Pokedex
{
    public class PokedexApi : IPokedexApi
    {
        private IPokedexCatalogue PokedexCatalogue;
        
        public PokedexApi(
            IPokedexCatalogue pokedexCatalogue)
        {
            PokedexCatalogue = pokedexCatalogue;
        }   
    }
}