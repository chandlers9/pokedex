To run
- Install all nuget packages
- Build
- Run

What I Would Do To Complete The project
In the PokeDex Project
Controller:
- Receive requests, route to relevant api layer function
- Unpack the Either abstraction and handle failures
- Formulate expected response if in a Right state

Api:
- Parameter validation (would be more important if this project was backed by it's own DB)
- Convert parameters to internal catalogue DTO 
- Convert catalogue response into external DTO (only expose fields that are intended for external viewing)
Catalogue:
- Main business logic goes here
- Makes calls to whatever data views are required and re-combines data as needed
View:
- Makes calls to "external" data sources, e.g. the PokeApiClient (could also be DB provider for example)
- Take api response object and extract relevant fields to internal catalogue DTO which is passed back to catalogue

Testing:
- Unit tests with mocks on all layers and validation
- Integration tests with mocked PokeApiClient responses

Would additionally want to add a translation service (similarly to how the PokeApiClient project has been added) for use in endpoint 2

Notes on choices made
- Making a standalone PokedexApiClient class library project 
- Provides decoupling between main project and potentially re-usable component
- Going to make my own HTTP requests to the endpoint rather than use a wrapper
- Removes the dependency on the maintainers of the wrapper (some of which you can see are no longer maintained anyway!)
- Only need access to a very small subsection of the Pokedex api, so seems 
- pointless installing a whole package just for that (and increasing the overall application size) 


Things that I might do differently in production
- Swagger Tests:
- Tests to ensure swagger is generating operation ids properly
- Tests that compare the dynamically generated swagger against a persisted version to prevent accidental modification to the api contract
- Use an explicitly run test to re-record the persisted version when making a change purposefully

- If the project was being deployed at each merge, may well develop “backwards” so that not exposing the controller until it is ready for use
- Would add system integration tests which spin up a servers + make http requests
- May implement more failures to be returned for different response codes in the PokeApiClient
