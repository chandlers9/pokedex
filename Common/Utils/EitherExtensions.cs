using System;
using Common.Failures;
using LanguageExt;

namespace Common.Utils
{
    public static class EitherExtensions
    {
        public static TRight ThrowIfFailed<TLeft, TRight>(
            this Either<TLeft, TRight> either)
            where TLeft : IFailure
        {
            return either.Match<TRight>(
                Right: right => right,
                Left: failure => throw new Exception("Failed!"));
        }
    }
}