using System.Net;

namespace Common.Failures
{
    public class UnsuccessfulResponseFailure : IFailure
    {
        public string Message { get; }
        
        public HttpStatusCode StatusCode { get; }
        
        public UnsuccessfulResponseFailure(
            string message,
            HttpStatusCode statusCode)
        {
            Message = message;
            StatusCode = statusCode;
        }
    }
}