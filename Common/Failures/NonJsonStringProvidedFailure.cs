namespace Common.Failures
{
    public class NonJsonStringProvidedFailure : IFailure
    {
        public string Message { get; }
        
        public string ProvidedValue { get; }

        public NonJsonStringProvidedFailure(
            string functionName,
            string providedValue)
        {
            Message = $"Non JSON string provided to {functionName}";
            ProvidedValue = providedValue;
        }
    }
}