namespace Common.Failures
{
    public interface IFailure
    {
        string Message { get; }
    }
}